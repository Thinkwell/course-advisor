#This script produces a formatted HTML file containing human-readable course review aggregate data for the Course Advisor project. 
#To run this script, download and install Python 3.6, and add the packages pandas, math, and codecs. (The Internet can explain how.)
#Before feeding the data, make sure that all courses are named consistently. E.g. "Comparative Politics" and "Introduction to Comparative Politics" will yield two different aggregates.
#(Upper/lower case, and spaces at the beginning/end of course names do not matter, the script will deal with them.)
#Last updated: 2018 October 17
#Corresponding author: Soem Zeijlmans, e-mail: soem@pm.me. Class of 2020.

#Select the spreadsheet with survey responses here. Make sure this script is located in the same folder as the spreadsheet. This script can only read the csv format.
ImportFile = 'Example.csv'

#PART 1 - PREPARING THE DATASET FOR USE

import pandas as pd #To manage the dataframe
import math #To determine whether a value is a number or not.
import codecs #To give the output the proper look (utf-8 used)

majors = ["GED", "EES", "GPH", "WP", "IJ", "HD"] #For assessing the use of a course per major, later on.

df = pd.read_csv(ImportFile, sep=',')

#Rename column names for easier use
df.rename(columns={'What course are you reviewing?': 'Course', 
                   'In what academic year did you take this course?': 'AcYear', 
                   'What is your major?': 'Major', 
                   'Is this course more practical or theoretical?': 'PracTheo', 
                   'In this course I...': 'Learned', 
                   'Do you agree? "I feel like this course thaught me something valuable for life."': 'Valuable', 
                   'Is this course useful for other subjects in your major?': 'Useful', 
                   'How is workload distributed throughout the course? [Pre-midterm week]': 'Workload1', 
                   'How is workload distributed throughout the course? [Midterm week]': 'Workload2', 
                   'How is workload distributed throughout the course? [Post-midterm week]': 'Workload3', 
                   'How is workload distributed throughout the course? [Reading week]': 'Workload4', 
                   'Is it a good idea to combine this course with courses that have a higher than average workload?': 'Combination', 
                   'In what year(s) would you recommend students to take this course?': 'ReccYears', 
                   'Do you have any tips or tricks to share to prospective students of this course?': 'Tips'}, inplace=True)

#Strip spaces at the beginning and the end of the open questions.
df.Course = df.Course.str.strip()
df.Tips = df.Tips.str.strip()

#Convert to uppercase to avoid case problems
df.Course = df.Course.apply(lambda x: x.upper())

#Simplify answers about whether the course should be combined with tough courses - saves typing later.
df['Combination'] = df['Combination'].str.replace("Yes, you'll survive.","Yes")
df['Combination'] = df['Combination'].str.replace("No, I would recommend taking it another time.","No")
df['Combination'] = df['Combination'].str.replace("Maybe, if you like working hard.","Maybe")

#Convert string workload data (Low/Middle/High) to numeric values so an average can later be calculated.
#The average (not mode) is chosen so if person A says "Low" and person B say "High", the result is "Middle".
for ctype in ['Workload1', 'Workload2', 'Workload3', 'Workload4']:
    df[ctype] = df[ctype].str.replace("Low","0")
    df[ctype] = df[ctype].str.replace("Middle","1")
    df[ctype] = df[ctype].str.replace("High","2")
    df[ctype] = pd.to_numeric(df[ctype], errors='coerce')
    
#Create a list of unique courses for later use
UniCourses = df['Course'].drop_duplicates().values.tolist()
#print (UniCourses)

#PART 2 - GENERATING AGGREGATE DATA AND WRITING HTML OUTPUT FILE
out = codecs.open("Overview_"+ImportFile+".html","w","utf-8") #utf-8 was chosen for a wider range of available characters, such as the star symbol.
out.write("""
<html>
<head>
<meta charset="utf-8"/>
<title>LUC Course Advisor Survey Results</title>
</head>
<body>""")#Head of the HTML file.

for i, course in enumerate(UniCourses): #Iterates over each course in the list of unique courses.
    grouped = df.groupby('Course')
    dfT = grouped.get_group(course)

    Responses = dfT.shape[0] #Counts the number of responses for this course

    TheoAv = int(round(dfT['PracTheo'].mean())) #Average of how theoretical the course is
    PracAv = 5-TheoAv #Average of how practical the course is - opposite of theoretical
    LearnedAv = int(round(dfT['Learned'].mean())) #Average of how much students learned in this course
    ValuableAv = int(round(dfT['Valuable'].mean())) #Average of whether they thought is was valuable for life

    UsefulAv = {'EES': 0, 'GPH': 0, 'GED': 0, 'HD': 0, 'IJ': 0, 'WP': 0} #Contains the average of how useful students find the course in their major. Needs a reset each iteration or data from previous courses will 'stick' to the next course.
    UsefulSS = {'EES': 0, 'GPH': 0, 'GED': 0, 'HD': 0, 'IJ': 0, 'WP': 0} #Contains the number of replies for this course per major students are in.
    groupedbymajor = dfT.groupby('Major') #Groups survey responses of a certain course by major.

    for key in UsefulAv: #Loops over different majors
        if key in dfT.Major.tolist():
            dfTM = groupedbymajor.get_group(key) #Creates a new DF with responses from a certain courses filled in by people in a certain major/
            UsefulAv.update({key: int(round(dfTM['Useful'].mean()))}) #Writes how useful it was to the dictionary specified above.
            UsefulSS.update({key: dfTM.shape[0]}) #Writes the number of responses (dfTM.shape[0]) to the other dictionary
        else:
            UsefulAv.update({key: 'NaN'}) #If there are no responses, 'NaN' is written to the dictionary to indicate that there's no data.

    def NumToWord(num): #A function that converts number values [0,1,2] back to words. Better readable for humans.    
        if num == 0:
            return "Low"
        elif num == 1:
            return "Middle"
        elif num == 2:
            return "High"

    periods = ['Workload1', 'Workload2', 'Workload3', 'Workload4'] #Respectively pre-midterm, midterm, post-midterm, reading week.
    Workload = []
    for period in periods:
        ThisWorkload = dfT[period].mean() #Takes the mean of the (numeric) workload indicator for a certain period
        if math.isnan(ThisWorkload):
            Workload.append("unknown") #If the result is not a number, it is unknown.
        else:
            Workload.append(NumToWord(int(round(ThisWorkload)))) #If the result is a number, add the workload to a list. Position 0 being workload 1, etc.

    CombiYes = 0 #Set to zero, because if the following if-statement is false, the data should not reflect the last course.
    CombiMaybe = 0
    CombiNo = 0
    CombiYesQ = 0 
    CombiMaybeQ = 0
    CombiNoQ = 0
    
    #This chunk of code calculates the percentage of respondents who (a) would (b) would not or (c) would maybe recommend taking this course with known heavy courses.
    if 'Yes' in dfT.Combination.tolist(): #This conditional is necessary because value_counts() returns a KeyError when there are no occurences
        CombiYesQ = dfT['Combination'].value_counts()['Yes'] #Counts the occurence of 'yes'
        CombiYes = (int(round(  dfT['Combination'].value_counts()['Yes']/Responses*100  ))) #And calculates its percentage
    if 'Maybe' in dfT.Combination.tolist():
        CombiMaybeQ = dfT['Combination'].value_counts()['Maybe']
        CombiMaybe = (int(round( dfT['Combination'].value_counts()['Maybe']/Responses*100 )))
    if 'No' in dfT.Combination.tolist():
        CombiNoQ = dfT['Combination'].value_counts()['No']
        CombiNo = (int(round(dfT['Combination'].value_counts()['No']/Responses*100)))

    RY1 = 0 #Set to zero, because if the following if-statement is false, the data should not reflect the last course.
    RY2 = 0
    RY3 = 0
    RY1Q = 0
    RY2Q = 0
    RY3Q = 0

    #RYn is the percentage of respondents who would recommend taking this course in year n.
    if 'First year' in str(dfT.ReccYears.tolist()): #This conditional is necessary because value_counts() returns a KeyError when there are no occurences
        RY1 = int(round(str(dfT['ReccYears']).count('First year')/Responses*100)) #Counts to occurences in the column converted to a string, because surveyees can choose multiple options.
        RY1Q = str(dfT['ReccYears']).count('First year')
    if 'Second year' in str(dfT.ReccYears.tolist()): 
        RY2 = int(round(str(dfT['ReccYears']).count('Second year')/Responses*100))
        RY2Q = str(dfT['ReccYears']).count('Second year')
    if 'Third year' in str(dfT.ReccYears.tolist()): 
        RY3 = int(round(str(dfT['ReccYears']).count('Third year')/Responses*100))
        RY3Q = str(dfT['ReccYears']).count('Third year')
        
    TipsList = dfT.Tips.tolist() #TipsList is a list of tips that people left for a course.
    TipsList = [x for x in TipsList if str(x) != 'nan'] #Remove empty entries.
    if TipsList == []:
        TipsList = ["Unfortunately we didn't get any tips or tricks on this course. If you have any, reach out to us!"]

    #Usefulnesses is a multi-line string containing the usefulness of a course per major.
    Usefulnesses = [] #Set to [] so it doesn't use the data from the previous course in the loop.
    for major in majors:
        if UsefulSS[major] > 0:
            Usefulnesses.append(f"""{major}: {"★"*UsefulAv[major]} ({UsefulAv[major]}/5, {UsefulSS[major]} responses)""")
    if Usefulnesses == []:
        Usefulnesses = ["Sorry, we don't have sufficient data on this."]
    
    #Write to the HTML output file. Using the new Python 3.6 f-strings, it's possible to insert variables into a multi-line string using f"""{variable}"""
    out.write(f"""<h1>{course}</h1>
    <i>Based on {Responses} responses.</i><br><br>
    <b>Is this course more practical or theoretical?</b><br>
    {PracAv}/5 practical, {TheoAv}/5 theoretical<br>
    <br>
    <b>To what extent do students feel like they learn a lot in this course?</b><br>
    {"★"*LearnedAv} ({LearnedAv}/5)<br>
    <br>
    <b>Do students think that this course is valuable in your life?</b><br>
    {"★"*ValuableAv} ({ValuableAv}/5)<br>
    <br>
    <b>How do students think that this course is helpful for subjects in their major?</b><br>
    """)
    out.write("{}".format("<br>".join(Usefulnesses)))
    out.write(f"""
    <br><br>
    <b>Workload distribution according to students:</b><br>
    Before midterm: {Workload[0]}<br>
    During midterm: {Workload[1]}<br>
    After midterm: {Workload[2]}<br>
    Reading week: {Workload[3]}<br>
    <br>
    <b>Do students think that you can combine this course with a tough course?</b><br>
    Yes, no problem: {CombiYes}% ({CombiYesQ} responses)<br>
    Maybe, if you like working hard: {CombiMaybe}% ({CombiMaybeQ} responses)<br>
    No, take it another time: {CombiNo}% ({CombiNoQ} responses)<br>
    <br>
    <b>In what year(s) do students recommend taking this course?</b><br>
    First year: {RY1}% ({RY1Q} responses)<br>
    Second year: {RY2}% ({RY2Q} responses)<br>
    Third year: {RY3}% ({RY3Q} responses)<br>
    <br>
    <b>Tips and tricks from students who have taken this course:</b><br>
    """)
    out.write("{}".format("<br>".join(TipsList)))

out.write("<br><br>(END)</body></html>")    
out.close()

